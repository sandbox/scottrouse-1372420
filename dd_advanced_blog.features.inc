<?php
/**
 * @file
 * dd_advanced_blog.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function dd_advanced_blog_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function dd_advanced_blog_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}

/**
 * Implements hook_node_info().
 */
function dd_advanced_blog_node_info() {
  $items = array(
    'blog_post' => array(
      'name' => t('Blog Post'),
      'base' => 'node_content',
      'description' => t('A <em>Blog Post</em> is a dated, journal-like article on your website.'),
      'has_title' => '1',
      'title_label' => t('Post Title'),
      'help' => '',
    ),
  );
  return $items;
}
